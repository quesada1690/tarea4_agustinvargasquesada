/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2semana3_agustinvargas;

import java.util.Scanner;

/**
 *
 * @author varga
 */
public class Tarea2Semana3_AgustinVargas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
                int opcion;
        Scanner eleccion = new Scanner(System.in);
        do  //menu con do/while para poder enciclar y que se salga hasta que el usuario quiera
        {
        System.out.println("Menú principal");
        System.out.println("Digite la opcion que desee utilizar");
        System.out.println("1-Matriz");
        System.out.println("2-Diccionario");
        System.out.println("3-Salir");
        opcion = eleccion.nextInt();
            switch (opcion)
                {
                    case 1:
                       clase_matriz matriz = new clase_matriz();
                       matriz.matriz_random();
                        break;
                    
                    case 2:
                        clase_diccionario diccionario = new clase_diccionario();
                        diccionario.diccionario_nombres();
                        break;
                }
        }while(opcion != 3);
        
        System.out.println("Que tenga un buen dia");
    }
    
}
