package tarea2semana3_agustinvargas;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author varga
 */
public class clase_matriz 
{
    public void matriz_random()
    {
        Random numero_random = new Random();
        int fila = 0;
        int columna = 0;
        int contador = 1;
        int numero_5 = 5;
        Scanner extencion_matriz = new Scanner(System.in);
        System.out.println("Digite la cantidad de filas que desee en la matriz");
        fila = extencion_matriz.nextInt();
        System.out.println("Digite la cantidad de columnas que desee en la matriz");
        columna = extencion_matriz.nextInt();
        int [][] numeros = new int[fila][columna];
        for (int x = 0; x < numeros.length; x++) 
        { // recorre las filas
            for (int y = 0; y < numeros[x].length; y++) 
            {//recorre las columnas, X es cada fila
                Scanner sc = new Scanner(System.in);
                numeros[x][y] = numero_random.nextInt(20);
                System.out.println(numeros[x][y]);
                if (numeros[x][0] == 5)
                {
                    System.out.println("La cantidad de numeros 5 en la primer fila es de " + contador);
                    contador = contador++;
                }            
            }    
        }
        System.out.println("La cantidad de numeros 5 en las filas son de " + contador);
    }
}
