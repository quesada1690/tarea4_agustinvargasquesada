package tarea2semana3_agustinvargas;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author varga
 */
public class clase_diccionario {
    
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLUE = "\u001B[34m";
    
    public void diccionario_nombres()
    {
        String persona; 
        int cantidad ,  edad;
        Map<String, Integer> primerDiccionario = new HashMap();
        Scanner datos = new Scanner(System.in);
        Scanner tamaño = new Scanner(System.in);
        System.out.println("Digite el tamaño del diccionario");
        cantidad =  tamaño.nextInt();
        for (int i = 1; i <= cantidad; i++)
        {
            System.out.println("Digite el nombre de la persona ");
            persona =  datos.next();
            System.out.println("Digite la edad de la persona ");
            edad =  datos.nextInt();
            primerDiccionario.put(persona , edad);
        }
        System.out.println(primerDiccionario);
        Set<String> llaves = primerDiccionario.keySet();
        for (String llave : llaves) 
        {
           edad = primerDiccionario.get(llave);
           if ((edad % 2) == 0 )
           {
               System.out.println(ANSI_BLUE + " "+ edad + ANSI_RESET);
           }
           else 
           {
               System.out.println(ANSI_RED + " "+ edad + ANSI_RESET);
           }
        }
    }
}
